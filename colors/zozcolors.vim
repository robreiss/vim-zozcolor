" Zoz Vim color scheme

set background=dark
hi clear

if exists("syntax_on")
  syntax reset
endif

let g:colors_name = "zozcolors"

" removes chars from split seperators and stuff
set fillchars=vert:\\
set fillchars=stl:\\
set fillchars=stlnc:\\

hi Normal                   guifg=#E6E1DC guibg=#000000 ctermfg=15 ctermbg=0
hi Comment                  guifg=#BC9458 ctermfg=137
hi Search                   guibg=#5A647E ctermbg=60
"Visual mode, ie highlighted text
hi Visual                   guibg=#555555 ctermbg=189

" set colorcolum=80 ColorColumn is then drawn as a page width indicator
hi ColorColumn              guibg=#001010 ctermbg=0
hi LineNr                   guifg=#888888 ctermfg=242
hi Cursor                   guifg=#000000 guibg=#FFFFFF ctermfg=0 ctermbg=15

" used in CtrlP for highlighted result
hi CursorLine               guifg=#000000 guibg=#A5C261 ctermfg=0 ctermbg=112
"" only used if cursorline is on and allows for highlighting of line number col
hi CursorLineNr             guifg=#888888 ctermfg=242
hi StatusLine               guifg=#333333 guibg=#99CC99 ctermfg=236 ctermbg=114
hi StatusLineNC             guifg=#333333 guibg=#888888 ctermfg=236 ctermbg=242
"hi! link CursorColumn ColorColumn
hi VertSplit                guifg=#333333 guibg=#888888 ctermfg=236 ctermbg=242
hi SignColumn               guifg=#FFFFFF ctermfg=15

" Invisible Characters
" for empty line tilde
hi NonText                  guifg=#888888 guibg=#000000 ctermfg=242 ctermbg=0
hi SpecialKey               guifg=#888888 guibg=#000000 ctermfg=242 ctermbg=0

" Syntastic
hi SignColumn               guifg=#99CC99 guibg=#333333 ctermfg=114 ctermbg=236

" Folds
" -----
" line used for closed folds
hi Folded                   guifg=#F6F3E8 guibg=#444444 gui=NONE ctermfg=15 ctermbg=238 cterm=NONE
hi! link FoldColumn SignColumn

" Misc
" ----
" directory names and other special names in listings
hi Directory                 guifg=#A5C261 gui=NONE ctermfg=107 cterm=NONE

" Popup Menu
" ----------
" normal item in popup
hi Pmenu                     guifg=#F6F3E8 guibg=#444444 gui=NONE ctermfg=15 ctermbg=239 cterm=NONE
" selected item in popup
hi PmenuSel                  guifg=#000000 guibg=#A5C261 gui=NONE ctermfg=0 ctermbg=107 cterm=NONE
" scrollbar in popup
hi PMenuSbar                 guibg=#5A647E gui=NONE ctermfg=15 ctermbg=60 cterm=NONE
" thumb of the scrollbar in the popup
hi PMenuThumb                guibg=#AAAAAA gui=NONE ctermfg=15 ctermbg=248 cterm=NONE

" Code constructs
" ---------------
"hi Comment                   guifg=#BC9458 ctermfg=137 gui=italic
hi link Todo Comment
hi Constant                  guifg=#6D9CBE ctermfg=73
hi Define                    guifg=#CC7833 ctermfg=130
hi Delimiter                 guifg=#519F50 ctermfg=22
hi Error                     guifg=#FFFFFF guibg=#990000 ctermfg=221 ctermbg=88
hi WarningMsg                guifg=#800000 guibg=NONE ctermfg=1 ctermbg=NONE
hi Function                  guifg=#FFC66D gui=NONE ctermfg=221 cterm=NONE
hi Identifier                guifg=#D0D0FF gui=NONE ctermfg=189 cterm=NONE
hi Include                   guifg=#CC7833 gui=NONE ctermfg=130 cterm=NONE
hi Keyword                   guifg=#CC7833 gui=NONE ctermfg=130 cterm=NONE
hi Macro                     guifg=#CC7833 gui=NONE ctermfg=130 cterm=NONE
hi Number                    guifg=#A5C261 ctermfg=107
hi PreCondit                 guifg=#CC7833 gui=NONE ctermfg=130 cterm=NONE
hi Statement                 guifg=#CC7833 gui=NONE ctermfg=130 cterm=NONE
hi String                    guifg=#A5C261 ctermfg=107
hi Title                     guifg=#FFFFFF ctermfg=15
hi Type                      guifg=#DA4939 gui=NONE ctermfg=167 cterm=NONE
hi PreProc                   guifg=#E6E1DC ctermfg=254
hi Special                   guifg=#DA4939 ctermfg=167 

" Javascript
hi link Noise Keyword

" Diffs
" -----
hi DiffAdd                   guifg=#E6E1DC guibg=#519F50 ctermfg=254 ctermbg=22
hi DiffDelete                guifg=#E6E1DC guibg=#660000 gui=bold ctermfg=254 ctermbg=52 cterm=bold
hi DiffChange                guifg=#FFFFFF guibg=#870087 ctermfg=15 ctermbg=90
hi DiffText                  guifg=#FFFFFF guibg=#FF0000 gui=bold ctermfg=15 ctermbg=9 cterm=bold

hi diffAdded                 guifg=#A5C261 ctermfg=107
hi diffNewFile               guifg=#FFFFFF guibg=NONE gui=bold ctermfg=15 ctermbg=NONE cterm=bold
hi diffFile                  guifg=#FFFFFF guibg=NONE gui=bold ctermfg=15 ctermbg=NONE cterm=bold

" Ruby
" ----
hi pythonBuiltin             guifg=#6D9CBE gui=NONE ctermfg=73 cterm=NONE
hi rubyTodo                  guifg=#DA4939 guibg=NONE gui=bold ctermfg=167 ctermbg=NONE cterm=bold
"hi rubyClass                 guifg=#FFFFFF ctermfg=15
hi link rubyClass Define
hi rubyConstant              guifg=#DA4939 ctermfg=167
hi rubyInterpolation         guifg=#FFFFFF ctermfg=15
hi rubyBlockParameter        guifg=#D0D0FF ctermfg=189
hi rubyPseudoVariable        guifg=#FFC66D ctermfg=221
hi rubyStringDelimiter       guifg=#00D700 ctermfg=40
hi rubyInstanceVariable      guifg=#D0D0FF ctermfg=189
hi rubyPredefinedConstant    guifg=#DA4939 ctermfg=167
hi rubyLocalVariableOrMethod guifg=#D0D0FF ctermfg=189

" Mail
" ----
hi mailSubject               guifg=#A5C261 ctermfg=107
hi mailHeaderKey             guifg=#FFC66D ctermfg=221
hi mailEmail                 guifg=#A5C261 ctermfg=107 gui=italic cterm=underline

" Spell
" ----
hi SpellBad                  guifg=#D70000 guibg=NONE gui=undercurl ctermfg=160 ctermbg=NONE cterm=underline
hi SpellRare                 guifg=#D75F87 guibg=NONE gui=underline ctermfg=168 ctermbg=NONE cterm=underline
hi SpellCap                  guifg=#D0D0FF guibg=NONE gui=underline ctermfg=189 ctermbg=NONE cterm=underline
hi SpellLocal                guifg=#00FFFF guibg=NONE gui=undercurl ctermfg=51 ctermbg=NONE cterm=underline
hi MatchParen                guifg=#FFFFFF guibg=#005f5f ctermfg=15 ctermbg=23

" XML
" ---
hi xmlTag                    guifg=#E8BF6A ctermfg=179
hi xmlTagName                guifg=#E8BF6A ctermfg=179
hi xmlEndTag                 guifg=#E8BF6A ctermfg=179

" HTML
" ----
hi link htmlTag              xmlTag
hi link htmlTagName          xmlTagName
hi link htmlEndTag           xmlEndTag

" CoffeeScript
hi coffeeSpecialVar          guifg=#CB7B36

" Markdown
hi link markdownError Normal

" Log
hi LogError   guifg=#DA4939 guibg=NONE
hi LogSuccess guifg=#39DA49 guibg=NONE
hi LogWarn    guifg=#888888 guibg=NONE
hi LogFade    guifg=#888888 guibg=NONE
